Django==3.1.7
psycopg2-binary==2.8.6
python-decouple==3.4
channels==3.0.3
channels_redis==3.2.0
asgiref==3.3.1
requests==2.25.1 