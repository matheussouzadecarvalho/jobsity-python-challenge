from asgiref.sync import async_to_sync

from csv import DictReader

from django.conf import settings
from django.utils.timezone import now

from channels.consumer import SyncConsumer

import requests


class StockConsumer(SyncConsumer):
    def get_stock_data(self, content):
        STOCK_API_URL = getattr(settings, 'STOCK_API_URL', None)

        room_group_name = content.get('room_group_name', None)
        stock_code = content.get('stock_code', None)

        if not stock_code or not room_group_name or not STOCK_API_URL:
            return

        try:
            req = requests.get(STOCK_API_URL, params={
                's': stock_code.lower(),
                'f': 'sd2t2ohlcv',
                'h': '',
                'e': 'csv',
            })
        except requests.exceptions.RequestException:
            self.send_message(room_group_name, 'stock API is not ok')
            return

        csv_lines = req.text.splitlines()
        csv_reader = DictReader(csv_lines)

        stock_data = next(csv_reader, None)

        if type(stock_data) is not dict:
            self.send_message(room_group_name, 'got error when reading response')
            return

        stock_symbol = stock_data.get('Symbol', 'unknown')
        stock_value = stock_data.get('Close', None)

        try:
            stock_value = float(stock_value)
            message = f'{stock_symbol} quote is ${stock_value} per share'
        except ValueError:
            message = f"{stock_symbol} not found"

        self.send_message(room_group_name, message)

    def send_message(self, room_group_name, message):
        async_to_sync(self.channel_layer.group_send)(
            room_group_name,
            {
                'type': 'send_message',
                'sender_name': 'stock_bot',
                'message': message,
                'timestamp': now().isoformat(),
            }
        )