from asgiref.sync import sync_to_async, async_to_sync

from django.test import TestCase
from django.contrib.auth import get_user_model
from django.contrib.auth import login

from channels.auth import AuthMiddlewareStack
from channels.testing import WebsocketCommunicator
from channels.routing import URLRouter
from channels.db import database_sync_to_async

from chat.routing import websocket_urlpatterns
from chat.consumers import ChatConsumer

User = get_user_model()


class ChatTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.application = AuthMiddlewareStack(URLRouter(websocket_urlpatterns))
        cls.user1 = User.objects.create_user('u1', 'u1@jobsity.com', 'u1u1')
        cls.user2 = User.objects.create_user('u2', 'u2@jobsity.com', 'u2u2')
        cls.room_name = 'room1'

    async def test_connect_ws_without_user_in_scope(self):
        communicator = WebsocketCommunicator(self.application, f'ws/chat/{self.room_name}/')
        connected, subprotocol = await communicator.connect()

        assert not connected
        await communicator.disconnect()

    async def test_connect_ws_with_registered_user_in_scope(self):
        communicator = WebsocketCommunicator(self.application, f'ws/chat/{self.room_name}/')
        communicator.scope['user'] = self.user1
        connected, subprotocol = await communicator.connect()

        assert connected
        await communicator.disconnect()

    async def test_user1_send_data_user2_receive_data_on_the_same_room(self):
        communicator_user1 = WebsocketCommunicator(self.application, f'ws/chat/{self.room_name}/')
        await sync_to_async(self.user1.refresh_from_db)()
        communicator_user1.scope['user'] = self.user1
        connected_user1, subprotocol_user1 = await communicator_user1.connect()

        communicator_user2 = WebsocketCommunicator(self.application, f'ws/chat/{self.room_name}/')
        await sync_to_async(self.user2.refresh_from_db)()
        communicator_user2.scope['user'] = self.user2
        connected_user2, subprotocol_user2 = await communicator_user2.connect()

        assert connected_user1 and connected_user2

        await communicator_user1.send_json_to({
            'action': 'new_message',
            'message': 'message',
        })

        response = await communicator_user2.receive_json_from()
        response.pop('timestamp')

        assert response == {
            'type': 'new_message',
            'sender_name': 'u1',
            'message': 'message',
        }

        await communicator_user1.disconnect()
        await communicator_user2.disconnect()

    async def test_user1_send_data_user2_dont_receive_data_on_different_rooms(self):
        communicator_user1 = WebsocketCommunicator(self.application, f'ws/chat/{self.room_name}/')
        await sync_to_async(self.user1.refresh_from_db)()
        communicator_user1.scope['user'] = self.user1
        connected_user1, subprotocol_user1 = await communicator_user1.connect()

        communicator_user2 = WebsocketCommunicator(self.application, f'ws/chat/whatever/')
        await sync_to_async(self.user2.refresh_from_db)()
        communicator_user2.scope['user'] = self.user2
        connected_user2, subprotocol_user2 = await communicator_user2.connect()

        assert connected_user1 and connected_user2

        await communicator_user1.send_json_to({
            'action': 'new_message',
            'message': 'message',
        })

        assert await communicator_user2.receive_nothing()

        await communicator_user1.disconnect()
        await communicator_user2.disconnect()
