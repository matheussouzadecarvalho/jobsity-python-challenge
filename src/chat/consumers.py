from datetime import datetime

from asgiref.sync import async_to_sync
from channels.generic.websocket import JsonWebsocketConsumer
from channels.auth import login

from . import models


class ChatConsumer(JsonWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        self.actions_map = {
            'new_message': self.new_message,
            'get_last_messages': self.get_last_messages,
            'get_stock_data': self.get_stock_data,
        }

        super().__init__(*args, **kwargs)

    def connect(self):
        user = self.scope.get('user', None)
        if not user or user.is_anonymous:
            self.close()

        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = f'chat_{self.room_name}'

        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )

    def receive_json(self, content):
        action = content.get('action', '')
        action_func = self.actions_map.get(action, None)

        if action_func:
            action_func(content)

    def get_last_messages(self, content):
        last_messages = models.Message.objects.filter(
            room_name=self.room_group_name
        ).values(
            'author__username',
            'content',
            'timestamp'
        ).order_by('-timestamp')[:50][::-1]

        content = {
            'type': 'get_last_messages',
            'messages': [
                {
                    'sender_name': message['author__username'],
                    'message': message['content'],
                    'timestamp': message['timestamp'].isoformat(),
                }
                for message in last_messages
            ],
        }

        self.send_json(content)

    def get_stock_data(self, content):
        async_to_sync(self.channel_layer.send)(
            'stock-bot',
            {
                'type': 'get_stock_data',
                'room_group_name': self.room_group_name,
                'stock_code': content['stock_code'],
            }
        )

    def new_message(self, content):
        message = content.get('message')

        if not message:
            return

        user = self.scope['user']

        instance = models.Message.objects.create(
            author=user,
            content=message,
            room_name=self.room_group_name,
        )

        self.broadcast_message(user.username, instance.content, instance.timestamp.isoformat())

    def broadcast_message(self, sender_name: str, message: str, timestamp: str):
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name,
            {
                'type': 'send_message',
                'sender_name': sender_name,
                'message': message,
                'timestamp': timestamp
            }
        )

    def send_message(self, event):
        content = {
            'type': 'new_message',
            'sender_name': event['sender_name'],
            'message': event['message'],
            'timestamp': event['timestamp'],
        }

        self.send_json(content)
