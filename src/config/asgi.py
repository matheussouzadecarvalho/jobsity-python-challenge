"""
ASGI config for finchat project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')

django_asgi_app = get_asgi_application()

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, ChannelNameRouter, URLRouter
from channels.staticfiles import StaticFilesWrapper

import chat.routing
from stock.consumers import StockConsumer

application = ProtocolTypeRouter({
    'http': StaticFilesWrapper(django_asgi_app),
    'websocket': AuthMiddlewareStack(
        URLRouter(chat.routing.websocket_urlpatterns)
    ),
    'channel': ChannelNameRouter({
        'stock-bot': StockConsumer.as_asgi(),
    }),
})
