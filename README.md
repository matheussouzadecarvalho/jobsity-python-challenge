### A browser-based chat application that allows users to get stock quotes using a specific command

<hr />

<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li><a href="#built-with">Built With</a></li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#limitations">Limitations</a></li>
    <li><a href="#disclaimer">Disclaimer</a></li>
  </ol>
</details>


### Built With

* [Python 3.9+](https://www.python.org/)
* [Django 3.1.*](https://www.djangoproject.com/)
* [Channels 3.0.*](https://channels.readthedocs.io/en/stable/)
* [Postgres 12](https://www.postgresql.org/)
* [Redis 5](https://redis.io/)


## Getting Started

### Prerequisites

1. **Docker** and **docker-compose** installed
2. Enter your `SECRET_KEY` on `environments/development.env` file. (you can use [Djecrety](https://djecrety.ir/) to generate a strong key)

### Installation

After cloning this repo, go to the root directory and run:
```sh
docker-compose up --build -d
```

## Usage

After the installation process, the application should be up and running.

Only a (super) user was automatically created. You have to create the other ones on the [Django Admin Panel](http://localhost:8000/admin/).

The superuser credentials are:
```json
{
  "username": "admin",
  "password": "admin"
}
```

The initial page is the [Login Page](http://localhost:8000/auth/login/). After logging in, you just have to type the room name you want and the chat page must be loaded.


## Limitation

The application uses a `cookie-based` authentication per host, so you can't open multiple windows of the same browser and have different users logged in. You have to open another browser for that.


## Observation
1. **Relational databases** aren't so great to work with chat applications. **Non-relational databases** are better for it.

2. **requirements.txt** purely is not the better way to handle with python dependencies, but it works for small projects. [Poetry](https://python-poetry.org/), for example, is much more robust.